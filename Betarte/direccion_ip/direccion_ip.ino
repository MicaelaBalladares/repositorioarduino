//#include <UIPEthernet.h> // Librería Ethernet que usaré después con el módulo ENC28J60
#include <Ethernet.h> // Librería Ethernet estándar
 
#define ESPERA_RENOVACION_IP 60000 // Un minuto
 
unsigned long reloj;
byte direccion_mac[]={0x12,0x34,0x56,0x78,0x9a,0xbc}; // Dirección MAC inventada
byte estado_DHCP;
 
void setup()
{
  Serial.begin(9600);
  Ethernet.begin(direccion_mac);
  mostrar_direccion_ip();
  reloj=millis()+ESPERA_RENOVACION_IP;
}
 
void loop()
{
  if(millis()>reloj) // Tratar de renovar la IP cada ESPERA_RENOVACION_IP milisegundos
  {
    estado_DHCP=Ethernet.maintain();
    switch(estado_DHCP)
    {
       case 0:
         Serial.println("Sin cambios");
         break;
       case 1:
         Serial.println("Error al renovar la dirección IP");
         break;
       case 2:
         Serial.println("Dirección IP renovada correctamente");
         break;
       case 3:
         Serial.println("Error al reasignar la dirección IP");
         break;
       case 4:
         Serial.println("Dirección IP reasignada correctamente");
         break;
       default:
         Serial.println("Error desconocido");
    }
    mostrar_direccion_ip();
    reloj=millis()+ESPERA_RENOVACION_IP;
  }
}
 
void mostrar_direccion_ip()
{
  Serial.print("Dirección IP actual [");
  Serial.print(Ethernet.localIP()); // Mostrará la dirección IP asignada por el servidor DHCP
  Serial.println("]");
}
