#include <Servo.h> 
 
Servo myservo;  // Se crea el objeto myservo para controlar el servomotor 
//Declaración de variables
int led = 13;
int ret=15;
void setup() 
{ 
  pinMode(led, OUTPUT); // El pin número 13 del arduino es seteado como salida para el control del motor C.C.
  myservo.attach(9);  // Se usa el pin número 9 para el control del servo

} 
 
void loop() 
{ 
// Ciclo for la variable i inicia con 100 que será la posición inicial del servo y terminará en 155 como posición final
for (int i=100; i <= 155; i++){    
 myservo.write(i);// se va asignando el valor de la variable i para indicar al servo su posición actual
delay(50); //Pequeño retardo para el brazo, para que el servo no suba bruscamente y no tire la solución jabonoso del burbujero 
 }
digitalWrite(led, HIGH);//Activa el motor de corrinete continua 
 delay(3000);  // Por 3 segundos
 digitalWrite(led, LOW);// Luego lo apaga
 myservo.write(100);// Lleva al servo a la posición inicial donde esta la solución jabonosa
delay(2000);// Durante dos segundos
} 
