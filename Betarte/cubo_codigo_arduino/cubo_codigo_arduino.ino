int t=100; //Tiempo
int i,j,k,v; //Contadores

int pines[4][4]; //Columnas 0-15
int pis[4]={17,18,19,20}; //Pisos 17-20

int pant[4][4][3]; //Píxeles

void setup() 
{ 
    Serial.begin (9600);
 v=0; //Inicializamos el contador en el que vamos a contar el pin
 for(i=0;i<4;i++) //Vueltas por la X del cubo
 {
  
   for (j=0;j<4;j++) //Vueltas por la Z del cubo
   {  
    Serial.println (v);
     pines[i][j]=v; //Guardamos el número de pin en la array de pines
     pinMode(v,OUTPUT); //Abrimos el pin
     v++; //Pasamos al siguiente pin
   }
 }
 for(v=0;v<4;v++) //Cuatro vueltas, una por cada pin de "piso"
 {
     pinMode(1+v,OUTPUT); //Abrimos el pin de "piso"
 }
}

void loop() {  
 test(); //Escena de test
 delay(500);

}

void test() 
{
 reset(); //Reseteamos el cubo para poner todos los leds a 0
 
 //Encendido
   for(i=0;i<4;i++) //Vuelta por la Y del cubo
   {
     for (j=0;j<4;j++) //Vuelta por la X del cubo
     {  
       for(k=0;k<3;k++) //Vuelta por la Z del cubo
       {
         pant[i][j][k]=1; //Encendemos el "pixel" actual
         frame(); //Función de "pintado por pantalla"
       }
     }
   }
   
 //Apagado
   for(i=0;i<4;i++) //Vuelta por la Y del cubo
   {
     for (j=0;j<4;j++) //Vuelta por la X del cubo
     {  
       for(k=0;k<3;k++) //Vuelta por la Z del cubo
       {
         pant[i][j][k]=0; //Encendemos el "pixel" actual
         frame(); //Función de "pintado por pantalla"
       }
     }
   }
}

void frame() //Función de "pintado de pantalla"
{
 for(v=0;v<t;v++)//Vueltas para mostrar el fotograma
 {
   for(i=0;i<4;i++) //Vuelta por la Y del cubo
   {
     digitalWrite (pis[i],HIGH); //Encendemos el piso    
     for(j=0;j<4;j++) //Vuelta por la X del cubo
     {
       for(k=0;k<3;k++) //Vuelta por la Z del cubo
       {
         if(pant[0][j][k]!=1) //Comprobamos el estado del "pixel" actual
         { //Si está encendido
           digitalWrite(pines[j][k],HIGH); //lo encendemos
         }
         else
         { //Si está apagado
           digitalWrite (pines[j][k],LOW); //lo apagamos
         }
       }
     }
     digitalWrite (pis[i],LOW); //Apagamos el piso
   }
 }  
}

void reset() 
{
 for(i=0;i<4;i++) //Vuelta por la Y del cubo
 {
   for (j=0;j<4;j++) //Vuelta por la X del cubo
   {  
     for(k=0;k<3;k++) //Vuelta por la Z del cubo
     {
       pant[i][j][k]=0; //Apagamos el "pixel" actual
     }
   }
 }
 frame(); //Función de "pintado por pantalla"
}
