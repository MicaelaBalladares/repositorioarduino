-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-05-2018 a las 15:34:52
-- Versión del servidor: 5.5.39
-- Versión de PHP: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `cuboled`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `canciones`
--

CREATE TABLE IF NOT EXISTS `canciones` (
`id` int(11) NOT NULL,
  `nombre` text NOT NULL,
  `artista` varchar(50) NOT NULL,
  `genero` varchar(50) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=41 ;

--
-- Volcado de datos para la tabla `canciones`
--

INSERT INTO `canciones` (`id`, `nombre`, `artista`, `genero`) VALUES
(3, 'Dura', 'Daddy Yanke', 'Regaetoon'),
(4, 'Marinero', 'Maluma', 'Trap'),
(7, 'Despacito', 'Luis Fonsi', 'Pop Latino'),
(8, 'El clavo', 'Prince Royce', 'Bachata'),
(9, 'Loca', 'Duki', 'Trap'),
(10, 'Dorado', 'Ecko', 'Trap'),
(13, 'Mil Horas', 'Andres Calamaro', 'Rock'),
(14, 'Innocence', 'Avril Lavigne', 'Pop'),
(17, 'Lose Yourself', 'Eminem', 'Rap'),
(18, 'Angel', 'Aerosmith', 'Rock'),
(21, 'The Lazy Song', 'Bruno Mars', 'Pop'),
(22, 'Relax', 'Paulo', 'Trap'),
(25, 'Darte un beso', 'Prince Royce', 'Bachata'),
(26, 'Historias de barrio', 'Matias Garrica', 'Trap'),
(29, 'Maybe ', 'Janis Joplin', 'Bluss'),
(30, 'Loco', 'Romeo Santos', 'Bachata'),
(33, 'Tranquila', 'J.Balvin', 'Regaeeton'),
(34, 'Me duele amarte', 'Reik', 'Pop'),
(37, 'Ese maldito momento', 'No te va a gustar', 'Rock'),
(38, 'Inolvidable', 'Reik', 'Pop');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `canciones`
--
ALTER TABLE `canciones`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `canciones`
--
ALTER TABLE `canciones`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=41;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
