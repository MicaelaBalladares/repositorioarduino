int LED = 13 ;
int IR = 2; //Entrada digital conectada al sensor infrarrojo

bool state;
const int pinLED = 13; 

void setup()
{
  Serial.begin(9600);
 pinMode( LED, OUTPUT) ; // LED como salida
 pinMode( IR , INPUT) ; //Sensor infrarrojo como entrada
}

void loop()
{
  int soundDetected = digitalRead(IR); 
  Serial.println(soundDetected);
  if (soundDetected == 0)
  {
     
    digitalWrite(pinLED , HIGH);
    delay (5000);
    digitalWrite(pinLED, LOW);
    
  }
}
